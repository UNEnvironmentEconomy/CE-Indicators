#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Thu 31 May 2018 12:06:38 PM CEST

"""

import pandas as pd
import os
import matplotlib.pyplot as plt

data_dict_rules = {
    "Share of existing Circular Economy Jobs": None,
    "Percentage of local GDP allocated to Circular Economy activities": None,
    "City product per capita": ["IBSA", "8.1.1.2"],
    "City’s unemployment rate": ["IBSA", "7.2.1.1"],
    "Percentage of persons in full-time employment": ["IBSA", "7.5.2.1"],
    "Number of businesses per 100 000 population": ["IBSA", "8.2.1.2"],
    "Total electrical energy use per capita": ["IBSA", "12.3.3.1"],
    "Total end-use energy consumption per capita": ["IBSA", "12.3.1.1"],
    "Share of energy generation within the Brussels Region": ["BE", "2011-2014"],
    "Per capita consumption of tap water": ["IBSA", "12.2.1.1"],
    "Percentage of water loss": None,
    "Per capita generation of municipal solid waste (MSW)": ["IBSA", "12.2.2.1"],
    "Recycling rate": ["IBSA", "12.2.2.4"],
    "Per capita demand for imported construction materials": None,
    "Per capita generation of construction and demolition waste (CDW)": ["IBSA", "12.2.2.3"],
    "Distribution of Circular Economy jobs by population strata": None,
    "Per capita CO2 emissions": ["BE", "2011-2014"],
    "Water quality": ["BE", "2011-2014"],
    "Distribution of resources by population strata": None,
}

lables_dict = dict()
for ddr, key in data_dict_rules.items():
    if key:
        lables_dict[key[1]] = {'title': ddr, 'source': key[0]}

data_path = "./data"

import os
TABLES = dict()
for d in os.listdir(data_path):
    ext = d.split('.')[-1]
    if ext == 'xls' or ext == 'xlsx':
        number = d.split("_")[0]
        TABLES[number] = d

class Indicators():
    def __init__(self, plot_style = 'fivethirtyeight'):
        self.indicators = data_dict_rules
        self.tables = dict()
        self.population = _get_pop(self.get_table(
            ['IBSA', '1.2.3.1'], plot=False, header=[1,2]))
        plt.style.use(plot_style)
        self.print_indicators()

    def print_values(self):
        print("{:>8} {:<60} | {:<8} {}".format('', 'Indicator', 'Value', 'Unit'))
        print("="*90)
        for key, val in self.tables.items():
            print("{:>8} {:<60} | {:<8} {}".format(key, lables_dict[key]['title'], val['value'], val['unit']))

    def print_indicators(self):
        print("{:>6} | {:<11} | {}".format("source", "num", "indicator"))
        print("="*90)
        for key, val in self.indicators.items():
            if val:
                print("{:>6} | {:<11} | {}".format(*val, key))
            else:
                print("{:>6} | {:<11} | {}".format("*est", "-", key))
        print("-"*90)
        print("*est = estimated values")

    def read_data(self, file_path, IBSA_key_full, **kwargs):
        if not 'header' in kwargs:
            kwargs['header'] = [1]
        if not 'skip_footer' in kwargs:
            kwargs['skip_footer'] = 8
        data_file = pd.ExcelFile(file_path)
        if IBSA_key_full in data_file.sheet_names:
            data_table = data_file.parse(IBSA_key_full, **kwargs)
        return data_table

    def _get_table(self, val, **kwargs):
        if val:
            if val[0] == 'IBSA':
                IBSA_key_full = val[-1]
                IBSA_key = ".".join(val[-1].split('.')[0:2])
                file_path = os.path.join(data_path, TABLES[IBSA_key])
                data_table = self.read_data(file_path, IBSA_key_full, **kwargs)
            else:
                data_table = False
            return data_table
        else:
            return False

    def get_table(self, val, plot=True, unit=None, reload=False, **kwargs):
        if val[1] in self.tables.keys() and not reload:
            data_table = self.tables[val[1]]['data']
            unit = self.tables[val[1]]['unit']
            value = self.tables[val[1]]['value']
        else:
            data_table = self._get_table(val, **kwargs)
            if val[1] == '12.2.1.4':
                data_table, unit, value = _process_12214(data_table)
            elif val[1]  == '8.1.1.2': #gdp
                data_table, unit, value = _process_8112(data_table)
                unit += " / pop"
            elif val[1] == '7.2.1.1': #unemployment
                data_table = data_table.loc['Région de Bruxelles-Capitale']
                data_table = data_table.div(self.population).mul(100)
                unit = 'Unemployed People [%]'
                value = data_table.iloc[-1]
            elif val[1] == '7.5.2.1': #emplyment
                data_table, unit, value = _process_7521(data_table)
                data_table = data_table.div(self.population).mul(100)
                unit += " [%]"
            elif val[1] == '8.2.1.2': #businesses
                data_table, unit, value = _process_8212(data_table)
                data_table = data_table.div(self.population.loc[2016]).mul(1000)
            elif val[1] == '12.3.3.1': # electricity
                data_table = data_table.iloc[0]
                data_table = data_table.div(self.population).mul(1e9)
                unit = 'kWh / population'
                value = data_table.iloc[-1]
            elif val[1] == '12.3.1.1': # energy
                data_table = data_table.iloc[-1]
                data_table = data_table.div(self.population).mul(1e6)
                unit = 'kWh / population'
                value = data_table.iloc[-1]
            elif val[1] == '12.2.1.1': # water
                data_table = data_table.iloc[2]
                data_table = data_table.div(self.population)
                unit = 'm3 / population'
                value = data_table.iloc[-1]
            elif val[1] == '12.2.2.1': # waste
                data_table = data_table.iloc[14, 1:]
                data_table = data_table.div(self.population)
                unit = 'ton / population'
                value = data_table.iloc[-1]
            elif val[1] == '12.2.2.4': #recycling
                if '12.2.2.1' in self.tables.keys():
                    waste_data = self.tables['12.2.2.1']['data']
                else:
                    waste_data = self.get_table(['IBSA', '12.2.2.1'], plot=False)
                data_table, unit, value = _process_12224(data_table, waste_data, self.population)
            elif val[1] == '12.2.2.3': #cdw
                data_table.index = data_table.loc[:, 'Type de déchet']
                data_table = data_table.loc['Total Bruxelles-Propreté', 'Tonnage collecté']
                value = data_table
                data_table = pd.Series(data_table, index=[2015])
                data_table = data_table.div(self.population.loc[2015])
                unit = 'ton / population'
            else:
                value = False
            self.tables[val[1]] = dict()
            self.tables[val[1]]['data'] = data_table
            self.tables[val[1]]['unit'] = unit
            self.tables[val[1]]['value'] = value

        if plot:
            if not isinstance(plot, bool):
                ax = plot
            else:
                ax = False
            _plot_data(data_table, val[1], unit, ax=ax)

        return data_table

    def plot_indicators(self):
        all_tables = pd.DataFrame(index=[int(y) for y in range(2000, 2017)])
        num_plots = 10
        fig, (ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10) = plt.subplots(
            num_plots,1, figsize=(10, 3*num_plots), sharex=True)
        data_table = self.get_table(['IBSA', '7.2.1.1'], skip_footer=6, plot=ax1)
        data_table.name = 'unemployment'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '7.5.2.1'], plot=ax2, header=[1,2,3])
        data_table.name = 'employment'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '8.1.1.2'], plot=ax3)
        data_table.name = 'GDP'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '8.2.1.2'], header=[1,2,3], skip_footer=131, plot=ax4)
        data_table.name = 'businesses'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '12.3.3.1'], plot=ax5)
        data_table.name = 'electricity'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '12.3.1.1'], plot=ax6, skip_footer=7)
        data_table.name = 'energy'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '12.2.1.1'], plot=ax7)
        data_table.name = 'water'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '12.2.2.1'], plot=ax8)
        data_table.name = 'MSW'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '12.2.2.4'], plot=ax9)
        data_table.name = 'recycling'
        all_tables = all_tables.join(data_table)
        data_table = self.get_table(['IBSA', '12.2.2.3'], plot=ax10)
        data_table.name = 'CDW'
        all_tables = all_tables.join(data_table)
        ax3.set_xticks([int(year) for year in range(2000, 2018, 2)])
        ax3.set_xticklabels([str(int(year)) for year in range(2000, 2018, 2)]);

        plt.tight_layout()
        plt.savefig('FIGURES/indicators.png', dpi=300)

        return all_tables

def _plot_data(data_table, val, unit, ax=False, style='-'):
    if not ax:
        fig, ax = plt.subplots(figsize=(10,2))
    if data_table.shape[0] == 1:
        style = 'o'
    data_table.plot(ax=ax, style=style)
    try:
        if isinstance(data_table, pd.DataFrame):
            rolling = pd.rolling_mean(data_table, 4)
        elif isinstance(data_table, pd.Series):
            rolling = data_table.rolling(4).mean()
        rolling.plot(ax=ax, style="--")
    except:
        print("can't compute rolling mean")
        pass
    ax.set_title(lables_dict[val]['title'])
    ax.set_ylabel(unit)
    ax.set_xlabel("year")

def _process_12214(data_table):
    data_table = data_table.loc[data_table.iloc[:, 0] == 'Turbidite']
    data_table.index = data_table.loc[:,'Année']
    data_table = data_table.loc[:, [i for i in data_table.columns if 'Réservoir' in i]]
    data_table = data_table.replace("<0,2", 0.1)
    val = data_table.iloc[-1].mean()
    return(data_table, "TUD", val)

def _process_8112(data_table):
    data_table = data_table.iloc[0]
    val = data_table.iloc[-1]
    return(data_table, "EUR", val)

def _process_8212(data_table):
    data_table = data_table.iloc[-1]
    data_table = data_table.loc[(slice(None), slice(None), "Total")]
    data_table.index = data_table.index.droplevel(1)
    data_table.index = [2016]
    return(data_table, '# Businesses', data_table.iloc[0])

def _process_7521(data_table):
    data_table = data_table.loc['Région de Bruxelles-Capitale', (['Année', 'Total'])]
    data_table.index = data_table.iloc[:,0]
    data_table.index.name = 'year'
    data_table = data_table.loc[:, ("Total", slice(None), 'Total')]
    data_table.columns = data_table.columns.droplevel([1,2])
    data_table = data_table.loc[:, 'Total']
    val = data_table.iloc[-1]
    return(data_table, "Employed People", val)

def _process_12224(data_table, waste_data, population):
    data_table = data_table.loc['Région de Bruxelles-Capitale']
    data_table.index = [int(y) for y in data_table.loc[:, 'Année']]
    data_table = data_table.loc[:, [c for c in data_table.columns if 'Année' not in c]]
    data_table = data_table.replace(':', 0)
    data_table = data_table.sum(axis=1)
    data_table = data_table.div(waste_data.mul(population)).mul(100)
    val = data_table.loc[data_table > 0]
    val = val.iloc[-1]
    return(data_table, 'Recycling Rate [%]', val)

def _get_pop(data_table):
    data_table = data_table.loc['Région de Bruxelles-Capitale', (slice(None),'Total')]
    data_table.index = data_table.index.droplevel(1)
    return data_table
