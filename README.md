# CE-Indicators

Circular Economy Indicators for the city of Brussels

| source | num         | indicator                                                         |
| ------ | ----------- | ----------------------------------------------------------------- |
|   *est | -           | Share of existing Circular Economy Jobs                           |
|   *est | -           | Percentage of local GDP allocated to Circular Economy activities  |
|   IBSA | 8.1.1.2     | City product per capita                                           |
|   IBSA | 7.2.1.1     | City’s unemployment rate                                          |
|   IBSA | 7.5.2.1     | Percentage of persons in full-time employment                     |
|   IBSA | 8.2.1.2     | Number of businesses per 100 000 population                       |
|   IBSA | 12.3.3.1    | Total electrical energy use per capita                            |
|   IBSA | 12.3.1.1    | Total end-use energy consumption per capita                       |
|     BE | 2011-2014   | Share of energy generation within the Brussels Region             |
|   IBSA | 12.2.1.1    | Per capita consumption of tap water                               |
|  *est  | -           | Percentage of water loss                                          |
|   IBSA | 12.2.2.1    | Per capita generation of municipal solid waste (MSW)              |
|   IBSA | 12.2.2.4    | Recycling rate                                                    |
|   *est | -           | Per capita demand for imported construction materials             |
|   IBSA | 12.2.2.3    | Per capita generation of construction and demolition waste (CDW)  |
|   *est | -           | Distribution of Circular Economy jobs by population strata        |
|     BE | 2011-2014   | Per capita CO2 emissions                                          |
|     BE | 2011-2014   | Water quality                                                     |
|   *est | -           | Distribution of resources by population strata                    |
 
*est = Estimation (forthcomming)